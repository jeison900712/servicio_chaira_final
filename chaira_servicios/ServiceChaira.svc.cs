﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.Text;
using System.Web.Script.Serialization;

namespace chaira_servicios
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServiceChaira" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServiceChaira.svc o ServiceChaira.svc.cs en el Explorador de soluciones e inicie la depuración.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]   
    public class ServiceChaira : IServiceChaira
    {
        JavaScriptSerializer jss = new JavaScriptSerializer();
        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<string>));
        DataChairaDataContext datos = new DataChairaDataContext();
        
        public string iniciar_sesion(string login, string psw)
        {
           /* -- --- inicio sesion ---
            select (Persona.nombre + ' ' + Persona.apellido1+ ' ' + Persona.apellido2) as nombre, Usuario.usuario, Persona.id_persona from Usuario
            inner join Persona on usuario.persona_id_persona=Persona.id_persona
            where usuario.usuario='el.cordoba' and usuario.contrasena='eliana'
            --- fin inicio de sesion ------*/
            string rs, json;
           

            var login_usu = from usu in datos.Usuario
                            join person in datos.Persona on usu.Persona_id_persona equals person.id_persona
                            where usu.usuario1 == login && usu.contrasena == psw
                            select new
                            {
                                usuario = usu.usuario1,
                                id = person.id_persona,
                                nombre_usuario= String.Concat(' ' + person.nombre + ' ' + person.apellido1 + ' ' + person.apellido2)
                                

                            };
            
            foreach (var usuario in login_usu)
            {
                Console.Write(usuario);
                json = jss.Serialize(usuario);
                return rs = json;

            }

            return "error";

        }

        public List<string> lista_grupos(string login)
        {
            string json, json1;
            string res = "";
            List<string> lista = new List<string>();

            /*-- cargar materias ----

                select Asignatura.id_asignatura, Asignatura.nombre_asig from grupo 
                inner join Asignatura on grupo.id_asignatura=Asignatura.id_asignatura 
                inner join matricula on grupo.id_grupo=matricula.id_grupo
                inner join Docente on grupo.codigo_docente= Docente.codigo
                inner join Estudiante on matricula.codigo_estudiante=Estudiante.codigo
             * 
             * 
             * ES AQUI DNDE TE DIGO ESTE ES LA CONSULTA EN SQL SERVER  ES EL OR QUE FALTA 
                inner join Persona on Estudiante.id_persona=Persona.id_persona or Docente.id_persona=Persona.id_persona
                inner join Usuario on Persona.id_persona= usuario.persona_id_persona
                where Usuario.usuario='d.lopez' group by Asignatura.id_asignatura, Asignatura.nombre_asig;


            ----- fin cargar materias------*/
            JavaScriptSerializer jss1 = new JavaScriptSerializer();
            var grupos_usu = from grp in datos.Grupo
                             join asig in datos.Asignatura on grp.id_asignatura equals asig.id_asignatura
                             join matri in datos.Matricula on grp.id_grupo equals matri.id_grupo
                             join docen in datos.Docente on grp.codigo_docente equals docen.codigo
                             join estud in datos.Estudiante on matri.codigo_estudiante equals estud.codigo
                             join person in datos.Persona on estud.id_persona equals person.id_persona
                             // or docen.id_persona equals person.id_persona
                             join usu in datos.Usuario on person.id_persona equals usu.Persona_id_persona
                             where usu.usuario1 == login
                             group asig by new { asig.id_asignatura, asig.nombre_asig } into groupin
                             select groupin.FirstOrDefault();

            foreach (var usua in grupos_usu)
            {
                Console.WriteLine(usua);
                json = jss.Serialize(usua.id_asignatura);
                json1 = jss1.Serialize(usua.nombre_asig);
                res = json + json1;
                lista.Add(res);
            }

            return lista;

        }

        public List<string> lista_contactos(string login)
        {
           
            string json;
            string res="";
            List<string> lista = new List<string>();
            /*----- Lista de contactos general -------

                select Usuario.usuario from grupo 
                inner join Asignatura on grupo.id_asignatura=Asignatura.id_asignatura 
                inner join matricula on grupo.id_grupo=matricula.id_grupo
                inner join Docente on grupo.codigo_docente= Docente.codigo
                inner join Estudiante on matricula.codigo_estudiante=Estudiante.codigo
               * 
             * 
             * ES AQUI DNDE TE DIGO ESTE ES LA CONSULTA EN SQL SERVER  ES EL OR QUE FALTA 
             * 
             * inner join Persona on Estudiante.id_persona=Persona.id_persona or Docente.id_persona=Persona.id_persona
                inner join Usuario on Persona.id_persona= usuario.persona_id_persona
                where Usuario.usuario !='el.cordoba' group by Usuario.usuario;

            --- fin lista de contactos general ------*/

            var contactos = from grp in datos.Grupo
                            join asig in datos.Asignatura on grp.id_asignatura equals asig.id_asignatura
                            join matri in datos.Matricula on grp.id_grupo equals matri.id_grupo
                            join docen in datos.Docente on grp.codigo_docente equals docen.codigo
                            join estu in datos.Estudiante on matri.codigo_estudiante equals estu.codigo
                            join person in datos.Persona on estu.id_persona equals person.id_persona
                            // or docen.id_persona equals person.id_persona
                            join usu in datos.Usuario on person.id_persona equals usu.Persona_id_persona
                            where usu.usuario1 != login
                            group usu by new { usu.usuario1 } into grouping
                            select grouping.FirstOrDefault();


            foreach (var usua in contactos)
            {
                Console.WriteLine(usua);
                json = jss.Serialize(usua.usuario1);
                res = json;
                lista.Add(res);
            }
            return lista;
        }

        public List<string> contactos_grupos(string login, int codigo_asignatura)
        {
            string json;
            string res = "";
            List<string> lista = new List<string>();
            /*---- lista contactos por grupos ----- 
                select Usuario.usuario from grupo 
                inner join Asignatura on grupo.id_asignatura=Asignatura.id_asignatura 
                inner join matricula on grupo.id_grupo=matricula.id_grupo
                inner join Docente on grupo.codigo_docente= Docente.codigo
                inner join Estudiante on matricula.codigo_estudiante=Estudiante.codigo
             * 
             * 
             * * 
             * 
             * ES AQUI DNDE TE DIGO ESTE ES LA CONSULTA EN SQL SERVER  ES EL OR QUE FALTA 
                inner join Persona on Estudiante.id_persona=Persona.id_persona or Docente.id_persona=Persona.id_persona
                inner join Usuario on Persona.id_persona= usuario.persona_id_persona
                where Asignatura.id_asignatura=15 and Usuario.usuario !='el.cordoba' group by Usuario.usuario;
            ----- fin lista contactos por grupos -------*/

            var contacto_g = from grp in datos.Grupo
                             join asig in datos.Asignatura on grp.id_asignatura equals asig.id_asignatura
                             join matri in datos.Matricula on grp.id_grupo equals matri.id_grupo
                             join docen in datos.Docente on grp.codigo_docente equals docen.codigo
                             join estu in datos.Estudiante on matri.codigo_estudiante equals estu.codigo
                             join per in datos.Persona on estu.id_persona equals per.id_persona
                             // or docen.id_persona equals per.id_persona
                             join usu in datos.Usuario on per.id_persona equals usu.Persona_id_persona
                             where asig.id_asignatura == codigo_asignatura && usu.usuario1 != login
                             group usu by new { usu.usuario1 } into grouping
                             select grouping.FirstOrDefault();

            foreach (var usua in contacto_g)
            {
                Console.WriteLine(usua);
                json = jss.Serialize(usua.usuario1);
                res = json;
                lista.Add(res);
               
            }

            return lista;
         }



    }
}
