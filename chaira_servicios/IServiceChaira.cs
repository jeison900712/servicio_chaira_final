﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
namespace chaira_servicios
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServiceChaira" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServiceChaira
    {
        [OperationContract]
        [WebGet(UriTemplate="iniciar_sesion", RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
        string iniciar_sesion(string login, string psw);

        [OperationContract]
        [WebGet(UriTemplate = "lista_grupos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<string> lista_grupos(string login);

        [OperationContract]
        [WebGet(UriTemplate = "lista_contactos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<string> lista_contactos(string login);

        [OperationContract]
        [WebGet(UriTemplate = "contactos_grupos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<string> contactos_grupos(string login, int codigo_asignatura);
    }
}
